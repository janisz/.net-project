			_   _        _     ____               _              _   
		   | \ | |  ___ | |_  |  _ \  _ __  ___  (_)  ___   ___ | |_ 
		   |  \| | / _ \| __| | |_) ||  __|/ _ \ | | / _ \ / __|| __|
		 _ | |\  ||  __/| |_  |  __/ | |  | (_) || ||  __/| (__ | |_ 
		(_)|_| \_| \___| \__| |_|    |_|   \___/_/ | \___| \___| \__|
											   |__/                  

#Create certificate
 * `Run Developer Command Prompt for VS2012` in Administrator Mode type:

	C:\Windows\system32>makecert.exe -sr LocalMachine -ss My -a sha1 -n CN=MyServerC
ert -sky exchange -pe
											   
#Requirements
 * [MongoDB](http://www.mongodb.org/downloads)
	- host: localhost
	- port: 27017
	- db: dotnet