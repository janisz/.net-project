﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common.DTO;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Collections.Generic;
using Common.OperationResult;

namespace Tests
{
    [TestClass]
    public class DataAccessTest
    {
        Server.MongoDataAccess dataAccess;
        Project testProject;
        Member testMember;
        Phase testPhase;

        public DataAccessTest()
        {
            dataAccess = new Server.MongoDataAccess();
            dataAccess.SetDbName("test");
            (dataAccess as Server.MongoDataAccess).DropDatabase();
        }

        [TestInitialize]
        public void SetUp()
        {           
            testProject = new Project(TestData.ProjectName);
            testPhase = new Phase(TestData.PhaseName);
            testMember = new Member(TestData.MemberName, TestData.Surname);
            testProject.Members.Add(testMember.ID);
            testProject.Phases.Add(testPhase);
        }

        [TestMethod]
        public void SetDbNameTest()
        {
            dataAccess.SetDbName("testName");
            Assert.AreEqual("testName", dataAccess.GetDbName());
        }

        [TestMethod]
        public void SaveProjectTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);

            var updatedProject = new Project(TestData.ProjectName);

            var clientsTimestamps = new Dictionary<ObjectId,DateTime>();
            clientsTimestamps.Add(testProject.ID, DateTime.MinValue);
            var updatedProjects = dataAccess.updatedProjects(clientsTimestamps);
            Assert.AreEqual(1, updatedProjects.Count);

            updatedProject.Timestamp = DateTime.MinValue;
            updatedProject.ID = testProject.ID;
            var result = dataAccess.updateProject(updatedProject);

            Assert.AreEqual(OperationResult.OutOfSync, result);  
        }

        [TestMethod]
        public void SaveNewMemberTest()
        {
            Member m = new Member(TestData.MemberName, TestData.Surname);
            dataAccess.saveMember(m);
            var actual = dataAccess.allMembers();

            Assert.AreEqual(m.Name, actual.First(_ => _.ID == m.ID).Name);
        }

        [TestMethod]
        public void AddNewProjectTest()
        {
            Member m = new Member(TestData.MemberName, TestData.Surname);
            dataAccess.saveMember(m);
            Project p = new Project(TestData.ProjectName);
            p.Members.Add(m.ID);
            dataAccess.addNewProjectForMember(p, m);

            var actual = dataAccess.projectsForMember(m);
            Assert.AreEqual(1, actual.Count);
            var actualProject = actual.First();
            Assert.AreEqual(p.Name, actualProject.Name);

            actual = dataAccess.projectsWithIDs(new List<ObjectId>() { p.ID });
            Assert.AreEqual(1, actual.Count);
            actualProject = actual.First();
            Assert.AreEqual(p.Name, actualProject.Name);

            actualProject = dataAccess.projectWithID(p.ID);
            Assert.AreEqual(p.Name, actualProject.Name);  
        }

        [TestMethod]
        public void UpdateProjectNameTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);

            testProject.Name = TestData.ProjectName;
            dataAccess.updateProject(testProject);

            var actualProject = dataAccess.projectWithID(testProject.ID);
            Assert.AreEqual(testProject.Name, actualProject.Name);  
        }

        [TestMethod]
        public void AddNewPhaseTest()
        {
            dataAccess.saveMember(testMember);
            testProject.Phases.Clear();
            dataAccess.addNewProjectForMember(testProject, testMember);
            var p = new Phase(TestData.PhaseName);
            dataAccess.addNewPhaseToProject(p, testProject);

            var actualProject = dataAccess.projectWithID(testProject.ID);
            Assert.AreEqual(p.ToJson(), actualProject.Phases.First().ToJson());
        }

        [TestMethod]
        public void UpdatePhaseTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);
            dataAccess.addNewPhaseToProject(testPhase, testProject);
            testPhase.Name = TestData.PhaseName;
            dataAccess.updatePhase(testPhase);

            var actualProject = dataAccess.projectWithID(testProject.ID);
            Assert.AreEqual(testPhase.ToJson(), actualProject.Phases.First().ToJson());
        }

        [TestMethod]
        public void RemovePhaseTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);
            dataAccess.addNewPhaseToProject(testPhase, testProject);
            dataAccess.removePhase(testPhase);
            var actualProject = dataAccess.projectWithID(testProject.ID);
            Assert.AreEqual(0, actualProject.Phases.Count);
        }

        [TestMethod]
        public void AddNewTaskAndMoveToAnotherPhaseTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);
            var p = new Phase(TestData.PhaseName);
            dataAccess.addNewPhaseToProject(p, testProject);

            var t = new Task(TestData.TaskName);
            t.Description = TestData.RandomText;

            dataAccess.addNewTaskToPhase(t, testPhase);
            dataAccess.moveTaskToPhase(t, p);

            var actualProject = dataAccess.projectWithID(testProject.ID);
            Assert.AreEqual(0, actualProject.Phases[0].TaskIDs.Count);
            Assert.AreEqual(1, actualProject.Phases[1].TaskIDs.Count);
        }

        [TestMethod]
        public void UpdateTaskTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);

            var t = new Task(TestData.TaskName);
            t.Description = TestData.RandomText;

            dataAccess.addNewTaskToPhase(t, testPhase);

            t.Name = TestData.TaskName;
            t.Description = "TestData.RandomText";

            dataAccess.updateTask(t);

            var actual = dataAccess.taskWithID(t.ID);
            Assert.AreEqual(t.Name, actual.Name);
            Assert.AreEqual(t.Description, actual.Description);
        }

        [TestMethod]
        public void GetTasksListTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);

            var t = new Task(TestData.TaskName);
            t.Description = TestData.RandomText;

            dataAccess.addNewTaskToPhase(t, testPhase);
            dataAccess.addMemberToTask(testMember, t);

            t.Name = TestData.TaskName;
            t.Description = "TestData.RandomText";

            dataAccess.updateTask(t);

            var actualList = dataAccess.tasksWithIDs(new List<ObjectId>() { t.ID });
            var actualMemberTasks = dataAccess.tasksForMember(testMember);
            Assert.AreEqual(1, actualList.Count);
            Assert.AreEqual(actualMemberTasks[0].ToJson(), actualList[0].ToJson());

            dataAccess.removeMemberFromTask(testMember, t);
            var actualTask = dataAccess.tasksWithIDs(new List<ObjectId>() { t.ID })[0];
            Assert.AreEqual(0, actualTask.Members.Count);
        }

        [TestMethod]
        public void RemoveTaskTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);

            var t = new Task(TestData.TaskName);
            t.Description = TestData.RandomText;

            dataAccess.addNewTaskToPhase(t, testPhase);
            dataAccess.addMemberToTask(testMember, t);
            dataAccess.removeTask(t);

            var actualList = dataAccess.tasksWithIDs(new List<ObjectId>() { t.ID });
            Assert.AreEqual(1, actualList.Count);
            Assert.AreEqual(null, actualList[0]);
        }

        [TestMethod]
        public void AddAndRemoveMemberToProjectTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);
            dataAccess.addMemberToProject(testMember, testProject);
            var actual = dataAccess.projectWithID(testProject.ID);

            Assert.AreEqual(testMember.ID, actual.Members[0]);

            dataAccess.removeMemberFromProject(testMember, testProject);
            actual = dataAccess.projectWithID(testProject.ID);

            Assert.AreEqual(0, actual.Members.Count);
        }

        [TestMethod]
        public void RemoveProjectTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);
            dataAccess.addMemberToProject(testMember, testProject);
            var t = new Task(TestData.TaskName);
            t.Description = TestData.RandomText;
            dataAccess.addNewTaskToPhase(t, testPhase);
            dataAccess.addMemberToTask(testMember, t);

            dataAccess.removeProject(testProject);

            var actual = dataAccess.projectWithID(testProject.ID);
            Assert.AreEqual(null, actual);

            var actualList = dataAccess.tasksWithIDs(new List<ObjectId>() { t.ID });
            Assert.AreEqual(1, actualList.Count);
            Assert.AreEqual(null, actualList[0]);
        }


        [TestMethod]
        public void UpdatedTasksTest()
        {
            dataAccess.saveMember(testMember);
            dataAccess.addNewProjectForMember(testProject, testMember);
            dataAccess.addMemberToProject(testMember, testProject);

            var clientsTimestampsMax = new Dictionary<ObjectId, DateTime>();
            var clientsTimestampsMin = new Dictionary<ObjectId, DateTime>();
            for (int i = 0; i < 10; i++)
            {
                var t = new Task(TestData.TaskName);
                t.Description = TestData.RandomText;
                dataAccess.addNewTaskToPhase(t, testPhase);
                dataAccess.addMemberToTask(testMember, t);
                clientsTimestampsMax.Add(t.ID, DateTime.MaxValue);
                clientsTimestampsMin.Add(t.ID, DateTime.MinValue);
            }

            var actual = dataAccess.updatedTasks(clientsTimestampsMax);
            Assert.AreEqual(0, actual.Count);
            
            actual = dataAccess.updatedTasks(clientsTimestampsMin);
            Assert.AreEqual(10, actual.Count);
        }

        [TestMethod]
        public void MemberLoginTest()
        {
            var login = testMember.Name + testMember.Surname[0];
            var pass  = TestData.RandomText;
            dataAccess.saveNewMemberWithLoginAndPassword(testMember, login, pass);
            var actual = dataAccess.memberWithLoginAndPassword(login, pass);
            Assert.AreEqual(testMember.ToJson(), actual.ToJson());
        }

    }

}

