﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests
{
    class TestData
    {
        static Random rand = new Random();

        #region Test's data
        static string[] names = new string[] {
            "Jacob", "Sophia", "Mason", "Isabella",
            "William", "Emma",  "Jayden", "Olivia",
            "Noah",  "Ava",  "Michael", "Emily",
            "Ethan", "Abigail", "Alexander", "Madison",
            "Aiden", "Mia",  "Daniel", "Chloe"};

        static string[] surnames = new string[] {
            "SMITH",	"JOHNSON",	"WILLIAMS",	"JONES",	"BROWN",
            "DAVIS",	"MILLER",	"WILSON",	"MOORE",	"TAYLOR",
            "ANDERSON",	"THOMAS",	"JACKSON",	"WHITE",	"HARRIS",
            "MARTIN",	"THOMPSON",	"GARCIA",	"MARTINEZ",	"ROBINSON"};
        #endregion

        #region public getters

        public static string MemberName {
            get { return names[rand.Next(names.Length)]; }
        }

        public static string Surname {
            get { return surnames[rand.Next(surnames.Length)]; }
        }

        public static string PhaseName {
            get { return NLipsum.Core.LipsumGenerator.Generate(rand.Next(1,3), NLipsum.Core.Features.Words, "", NLipsum.Core.Lipsums.TheRaven); }
        }

        public static string ProjectName {
            get { return NLipsum.Core.LipsumGenerator.Generate(rand.Next(1,3), NLipsum.Core.Features.Words, "", NLipsum.Core.Lipsums.TheRaven); }
        }

        public static string TaskName {
            get { return NLipsum.Core.LipsumGenerator.Generate(1, NLipsum.Core.Features.Sentences, "",NLipsum.Core.Lipsums.TheRaven); }
        }

        public static string RandomText {
            get
            {
                return NLipsum.Core.LipsumGenerator.Generate(1);
            }
        }

        #endregion

    }
}
