﻿using System;

using System.Windows.Data;
using System.Windows.Media;

namespace WPFFrontEnd.Misc
{
    /// <summary>
    /// Converts numeric priority value to it's color representation
    /// </summary>
    class PriorityColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int priority = (int)value;
            Color color = Colors.Red;

            switch (priority)
            {
                case 0:
                    color = Color.FromRgb(225, 225, 225);
                    break;
                case 1:
                    color = Color.FromRgb(255, 247, 153);
                    break;
                case 2:
                    color = Color.FromRgb(253, 198, 137);
                    break;
                case 3:
                    color = Color.FromRgb(247, 148, 29);
                    break;
                case 4:
                    color = Color.FromRgb(255, 0, 0);
                    break;
            }

            return new SolidColorBrush(color);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }


}
