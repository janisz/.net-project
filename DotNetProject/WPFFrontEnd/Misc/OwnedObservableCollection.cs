﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace WPFFrontEnd.Misc
{
    /// <summary>
    /// A simple extention of ObservableCollection allowing to access to owner of the collection on the CollectionChanged event,
    /// usefull in parent<->children relationship
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class OwnedObservableCollection<T> : ObservableCollection<T>
    {
        public OwnedObservableCollection()
        {
        }
        public OwnedObservableCollection(IEnumerable<T> collection)
            : base(collection)
        {
        }
        public OwnedObservableCollection(List<T> list)
            : base(list)
        {
        }

        public void AddWithoutNotification(T item)
        {
            _suppressNotification = true;
            base.Add(item);
            _suppressNotification = false;
        }

        private bool _suppressNotification = false;

        protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (!_suppressNotification)
                base.OnCollectionChanged(e);
        }


        object owner;

        public object Owner
        {
            get { return owner; }
            set { owner = value; }
        }
    }
}
