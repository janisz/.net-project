﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using WPFFrontEnd.Presentation;
using WPFFrontEnd.Controllers;

namespace WPFFrontEnd.Windows
{
    /// <summary>
    /// Application's MainWindow class
    /// </summary>
    public partial class MainWindow : Window
    {

        Controller _controller;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void PostLogin(string s)
        {
            this.Title = "Projectly - " + s;

            _controller.FetchStartupData(a => {
                this.IsEnabled = true;
                PostFetch(a);
            });
            _controller.UserProjects.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(UserProjects_CollectionChanged);
            this.DataContext = _controller.UserProjects;
        }

        private void ShowNewMemberDialog(string title)
        {
            NewMemberWindow newMember = new NewMemberWindow();
            newMember.statusLabel.Content = title;
            newMember.ShowDialog();

            if (newMember.DialogResult == true)
            {
                _controller.NewMemberWithCredentials(newMember.NameTextBox.Text, newMember.SurnameTextBox.Text, newMember.LoginTextBox.Text,
                newMember.PasswordTextBox.Password, (r, s) =>
                {
                    if (r)
                    {
                        PostLogin(s);
                    }
                    else
                    {
                        ShowNewMemberDialog(s);
                    }
                });

            }
            else
            {
                ShowLoginDialog("");
            }
        }

        private void ShowLoginDialog(string title)
        {
            LoginWindow login = new LoginWindow();
            login.titleLabel.Content = title;

            login.ShowDialog();
            if (login.DialogResult == true)
            {
                if (login.didSelectNewMember)
                {
                    ShowNewMemberDialog("");
                }
                else
                {
                    _controller.LoginWithCredentials(login.LoginTextBox.Text, login.PasswordTextBox.Password, (r, s) => {

                        if (r)
                        {
                            PostLogin(s);
                        } 
                        else
                        {
                            ShowLoginDialog(s);
                        }
                    });
                }
            }
            else
            {
                this.Close();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.IsEnabled = false;

            _controller = new Controller();

            ShowLoginDialog("");
        }

        #region Menu Clicks

        private void closeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        private void addNewProjectMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddNewProject();
        }

        #endregion



        #region Button Clicks

        private void taskInfo_Click(object sender, MouseButtonEventArgs e)
        {
            Border border = sender as Border;

            Task task = border.Tag as Task;

            TaskWindow taskWindow = new TaskWindow();
            taskWindow.DataContext = task;
            taskWindow.ShowDialog();
        }

        private void addTaskButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Phase phase = button.Tag as Phase;

            Task newTask = new Task("New task");
            newTask.Phase = phase; // hack for members list to display, 

            TaskWindow taskWindow = new TaskWindow();
            taskWindow.DataContext = newTask;
            taskWindow.ShowDialog();
            newTask.Phase = null; // hack continued...

            if (taskWindow.DialogResult == true)
            {
                _controller.addTaskToPhase(newTask, phase);
            }
        }

        private void addMemberButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Project project = button.Tag as Project;

            AddMemberWindow addWindow = new AddMemberWindow();
            var c = new ObservableCollection<Member>(_controller.Members.Except(project.Members));

            addWindow.DataContext = c;
            addWindow.Title = "Add members...";

            addWindow.ShowDialog();
            if (addWindow.DialogResult == true)
            {
                var a = addWindow.membersList.SelectedItems;

                foreach (Member item in a)
	            {
                    project.Members.Add(item);
	            }
            }
        }

        private void addPhaseButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Project project = button.Tag as Project;

            AddNewWindow addWindow = new AddNewWindow();
            addWindow.Title = "Add new phase...";
            addWindow.Subtitle = "Enter new phase name:";

            addWindow.ShowDialog();
            if (addWindow.DialogResult == true)
            {
                Phase phase = new Phase(addWindow.SelectedName);
                project.Phases.Add(phase);
            }
        }


        private void removePhaseButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Phase phase = button.Tag as Phase;

            MessageBoxResult result = MessageBox.Show("Do you want to remove phase \"" + phase.Name + "\"?", "Remove Phase", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                phase.Removed = true;
            }
        }

        private void removeProjectButton_Click(object sender, RoutedEventArgs e)
        {
            Project project = ((Button)sender).Tag as Project;

            MessageBoxResult result = MessageBox.Show("Do you want to remove project \"" + project.Name + "\"?", "Remove Project", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                project.Removed = true;
            }
        }

        #endregion

        #region Phase renaming

        private void confirmPhaseRenameClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Grid editGrid = VisualTreeHelper.GetParent(button) as Grid;
            Grid grid = VisualTreeHelper.GetParent(editGrid) as Grid;
            TextBox textBox = editGrid.FindName("phaseNameTextBox") as TextBox;

            textBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();

            setEditModeOnPhaseGrid(false, VisualTreeHelper.GetParent(editGrid) as Grid);
        }

        private void declinePhaseRenameClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Grid editGrid = VisualTreeHelper.GetParent(button) as Grid;
            TextBox textBox = editGrid.FindName("phaseNameTextBox") as TextBox;

            textBox.GetBindingExpression(TextBox.TextProperty).UpdateTarget();

            setEditModeOnPhaseGrid(false, VisualTreeHelper.GetParent(editGrid) as Grid);
        }


        private void phaseLabelClick(object sender, MouseButtonEventArgs e)
        {
            Label label = sender as Label;
            Grid grid = VisualTreeHelper.GetParent(label) as Grid;
            Grid editGrid = grid.FindName("phaseNameEditGrid") as Grid;

            setEditModeOnPhaseGrid(true, grid);
        }

        private void setEditModeOnPhaseGrid(bool editMode, Grid phaseGrid)
        {
            Grid normalGrid = phaseGrid.FindName("phaseNameGrid") as Grid;
            Grid editGrid = phaseGrid.FindName("phaseNameEditGrid") as Grid;

            normalGrid.Visibility = editMode ? Visibility.Collapsed : Visibility.Visible;
            editGrid.Visibility = editMode ? Visibility.Visible : Visibility.Collapsed;
        }

        #endregion


        void UserProjects_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                tabControl.SelectedIndex = e.NewStartingIndex;
            }
        }

        void AddNewProject()
        {
            AddNewWindow addWindow = new AddNewWindow();
            addWindow.Title = "Add new project...";
            addWindow.Subtitle = "Enter new project name:";

            addWindow.ShowDialog();
            if (addWindow.DialogResult == true)
            {
                Project project = new Project(addWindow.SelectedName);
                _controller.UserProjects.Add(project);
            }
        }

        private void PostFetch(bool fetchResult)
        {

            if (_controller.UserProjects.Count == 0)
            {
                MessageBoxResult result = MessageBox.Show("You don't seem to have any projects yet, would you like to create a new one?", "No projects", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    AddNewProject();
                }
            }
        }






    }
}
