﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFFrontEnd.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public bool didSelectNewMember = false;

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void checkTextBoxes()
        {
            this.loginButton.IsEnabled = this.LoginTextBox.Text.Length != 0 && this.PasswordTextBox.Password.Length != 0;

        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTextBoxes();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void newMemberButton_Click(object sender, RoutedEventArgs e)
        {
            didSelectNewMember = true;
            this.DialogResult = true;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            checkTextBoxes();
        }
    }
}
