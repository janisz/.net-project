﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFFrontEnd.Windows
{
    /// <summary>
    /// Interaction logic for NewMemberWindow.xaml
    /// </summary>
    public partial class NewMemberWindow : Window
    {
        public NewMemberWindow()
        {
            InitializeComponent();
        }

        private void exitButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void createButton_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }


        private void checkTextBoxes()
        {
            this.createButton.IsEnabled = this.LoginTextBox.Text.Length != 0 && this.PasswordTextBox.Password.Length != 0;

        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            checkTextBoxes();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            checkTextBoxes();
        }
    }
}
