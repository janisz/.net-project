﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using WPFFrontEnd.Presentation;

namespace WPFFrontEnd.Windows
{
    /// <summary>
    /// Interaction logic for TaskWindow.xaml
    /// </summary>
    public partial class TaskWindow : Window
    {
        public TaskWindow()
        {
            InitializeComponent();
        }


        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.taskNameTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            this.taskDescriptionTextBox.GetBindingExpression(TextBox.TextProperty).UpdateSource();
            this.priorityList.GetBindingExpression(ListBox.SelectedIndexProperty).UpdateSource();

            Task task = this.DataContext as Task;
            task.Members.Clear();

            foreach (var m in this.membersList.SelectedItems)
            {
                Member member = m as Member;
                task.Members.Add(member);
            }

            this.DialogResult = true;
            this.Close();
        }


        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {

            MessageBoxResult result = MessageBox.Show("Do you want to remove this task?", "Remove Task", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                Task task = this.DataContext as Task;
                task.Removed = true;

                this.DialogResult = false;
                this.Close();
            }
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Task task = this.DataContext as Task;

            foreach (var member in task.Members)
	        {
                membersList.SelectedItems.Add(member);
	        }
        }

    }
}
