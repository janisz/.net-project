﻿using AutoMapper;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using WPFFrontEnd.Misc;

using Common.OperationResult;

namespace WPFFrontEnd.Controllers
{
    /// <summary>
    /// This class is effectively a layer between GUI and DAL. It's responsible for monitoring changes in presenation models
    /// and sending sending those changes to the database
    /// </summary>
    class Controller
    {
        public IServerDataAccess dataProvider;

        const int kPullInterval = 5;

        public Common.DTO.Member CurrentUserDTO
        {
            get;
            set;
        }

        public Presentation.Member CurrentUserPres
        {
            get;
            set;
        }

        private ObservableCollection<Presentation.Project> userProjects = new ObservableCollection<Presentation.Project>();

        public ObservableCollection<Presentation.Project> UserProjects
        {
            get { return userProjects; }
            private set { userProjects = value; }
        }

        private ObservableCollection<Presentation.Member> members = new ObservableCollection<Presentation.Member>();

        public ObservableCollection<Presentation.Member> Members
        {
            get { return members; }
            private set { members = value; }
        }

        private Dictionary<string, Presentation.Project> _projects = new Dictionary<string, Presentation.Project>();
        private Dictionary<string, Presentation.Phase> _phases = new Dictionary<string, Presentation.Phase>();
        private Dictionary<string, Presentation.Task> _tasks = new Dictionary<string, Presentation.Task>();
        private Dictionary<string, Presentation.Member> _members = new Dictionary<string, Presentation.Member>();

        public Controller()
        {
            dataProvider = new WCFController();

        }

        public Controller(IServerDataAccess dataAccess)
        {
            dataProvider = dataAccess;
            MapModels();
            CurrentUserDTO = new Common.DTO.Member("Bartosz", "Ciech");
            CurrentUserDTO.ID = new ObjectId(1,1,1,1);
            //dataProvider.saveMember(CurrentUserDTO);
        }

        static public void MapModels()
        {
            // DTO -> Presentation

            Mapper.AllowNullDestinationValues = true;

            Mapper.CreateMap<Common.DTO.Model, Presentation.Model>()
                        .Include<Common.DTO.Project, Presentation.Project>()
                        .Include<Common.DTO.Phase, Presentation.Phase>()
                        .Include<Common.DTO.Task, Presentation.Task>()
                        .Include<Common.DTO.Member, Presentation.Member>()
                        .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID.ToString()))
                        .ForMember(dest => dest.Removed, opt => opt.Ignore());

            Mapper.CreateMap<Common.DTO.Project, Presentation.Project>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID.ToString()))
                .ForMember(dest => dest.Members, opt => opt.Ignore())
                .AfterMap((s, d) =>
                {
                    foreach (var phase in d.Phases)
                        phase.Project = d;
                });

            Mapper.CreateMap<Common.DTO.Member, Presentation.Member>()
                .ForMember(dest => dest.ID, opt => opt.MapFrom(src => src.ID.ToString()));

            Mapper.CreateMap<Common.DTO.Phase, Presentation.Phase>()
                .ForMember(dest => dest.Project, opt => opt.Ignore()) //ignoring
                .ForMember(dest => dest.Tasks, opt => opt.Ignore());

            Mapper.CreateMap<Common.DTO.Task, Presentation.Task>()
                .ForMember(dest => dest.Phase, opt => opt.Ignore())
                .ForMember(dest => dest.Members, opt => opt.Ignore());

            // Presentation -> DTO


            Mapper.CreateMap<string, ObjectId>().ConvertUsing(x =>
            {
                return new ObjectId(x);
            });

            Mapper.CreateMap<Presentation.Model, Common.DTO.Model>()
                .Include<Presentation.Project, Common.DTO.Project>()
                .Include<Presentation.Phase, Common.DTO.Phase>()
                .Include<Presentation.Task, Common.DTO.Task>();
                //.ForMember(dest => dest.ID, opt => opt.MapFrom(src => new ObjectId(src.ID)));

            Mapper.CreateMap<Presentation.Task, Common.DTO.Task>()
                .ConstructUsing(x => new Common.DTO.Task(x.Name))
                .ForMember(dest => dest.Members, opt => opt.Ignore())
                .ForMember(dest => dest.Timestamp, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(dest => dest.Order, opt => opt.MapFrom(src => src.Phase.Tasks.IndexOf(src)));

            Mapper.CreateMap<Presentation.Phase, Common.DTO.Phase>()
                .ConstructUsing(x => new Common.DTO.Phase(x.Name))
                .ForMember(dest => dest.TaskIDs, opt => opt.Ignore());

            Mapper.CreateMap<Presentation.Member, Common.DTO.Member>()
                .ConstructUsing(x => new Common.DTO.Member(x.Name, x.Surname))
                .ForMember(dest => dest.Projects, opt => opt.Ignore());


            Mapper.CreateMap<Presentation.Project, Common.DTO.Project>()
                .ConstructUsing(x => new Common.DTO.Project(x.Name))
                .ForMember(dest => dest.Phases, opt => opt.Ignore())
                .ForMember(dest => dest.Members, opt => opt.Ignore());
        
            Mapper.AssertConfigurationIsValid();
        }

        public void addTaskToPhase(Presentation.Task task, Presentation.Phase phase)
        {
            Presentation.Phase p = null;
            if (_phases.TryGetValue(phase.ID, out p))
            {
                p.Tasks.Add(task);
            }
        }

        static public void HandleException(Exception e)
        {
            System.Windows.MessageBox.Show(e.Message, e.Source);
        }

        private void MapDependenciesInProjectsPair(Common.DTO.Project sourceProj, Presentation.Project destProj)
        {
            shouldInformServer = false;

            destProj.Members.Clear();
            foreach (var item in sourceProj.Members)
            {
                Presentation.Member member = null;
                if (_members.TryGetValue(item.ToString(), out member))
                {
                    destProj.Members.Add(member);
                }
            }

            shouldInformServer = true;

            foreach (var phaseDTO in sourceProj.Phases)
            {
                var phasePres = destProj.Phases.First(p => p.ID.Equals(phaseDTO.ID.ToString()));

                List<ObjectId> missingTasks = new List<ObjectId>();

                foreach (var taskID in phaseDTO.TaskIDs)
                {

                    if (this._tasks.ContainsKey(taskID.ToString()))
                    {
                        shouldInformServer = false;
                        Presentation.Task task = _tasks[taskID.ToString()];
                        task.Phase = phasePres;
                        phasePres.Tasks.Add(task);
                        shouldInformServer = true;
                    }
                    else
                    {
                        missingTasks.Add(taskID);
                    }                   
                }

                if (missingTasks.Count != 0)
                {
                    dataProvider.fetchTasksWithIDsAsync(missingTasks, (r, l) =>
                    {
                        foreach (var taskDTO in l)
                        {
                            shouldInformServer = false;

                            var taskPres = Mapper.Map<Common.DTO.Task, Presentation.Task>(taskDTO);
                            taskPres.Phase = phasePres;
                            RemapMembersInTask(taskDTO, taskPres);

                            RegisterForTaskEvents(taskPres);

                            phasePres.Tasks.Add(taskPres);
                            shouldInformServer = true;
                        }

                        SortPhase(phasePres);

                    });
                }
            }
        }

        private void UnregisterForTaskEvents(Presentation.Task task)
        {
            task.PropertyChanged -= (task_PropertyChanged);
            task.Members.CollectionChanged -= TaskMembers_CollectionChanged;
        }

        private void RegisterForTaskEvents(Presentation.Task task)
        {
            task.PropertyChanged += new PropertyChangedEventHandler(task_PropertyChanged);
            task.Members.CollectionChanged += TaskMembers_CollectionChanged;
            _tasks[task.ID] = task;
        }

        private void UnregisterForProjectEvents(Presentation.Project project)
        {
            shouldInformServer = false;
            project.PropertyChanged -= project_PropertyChanged;
            project.Phases.CollectionChanged -= Phases_CollectionChanged;
            project.Members.CollectionChanged -= ProjectMembers_CollectionChanged;

            foreach (var phase in project.Phases)
            {
                phase.PropertyChanged -= phase_PropertyChanged;
                phase.Tasks.CollectionChanged -= Tasks_CollectionChanged;

                foreach (var task in phase.Tasks)
                {
                    UnregisterForTaskEvents(task);
                }
            }
            shouldInformServer = true;
        }

        private void RegisterForProjectEvents(Presentation.Project project)
        {
            shouldInformServer = false;

            project.PropertyChanged += project_PropertyChanged;
            project.Phases.CollectionChanged += Phases_CollectionChanged;
            project.Members.CollectionChanged += ProjectMembers_CollectionChanged;
            _projects[project.ID] = project;

            foreach (var phase in project.Phases)
            {
                phase.PropertyChanged += new PropertyChangedEventHandler(phase_PropertyChanged);
                phase.Tasks.CollectionChanged += new NotifyCollectionChangedEventHandler(Tasks_CollectionChanged);
                _phases[phase.ID] = phase;

                foreach (var task in phase.Tasks)
                {
                    RegisterForTaskEvents(task);
                }

                SortPhase(phase);
            }

            shouldInformServer = true;
        }


        private void ConsumeFetchedMembers(IList<Common.DTO.Member> fetchedMembers)
        {
            IList<Presentation.Member> members = Mapper.Map<IList<Common.DTO.Member>, IList<Presentation.Member>>(fetchedMembers);

            foreach (var member in members)
            {
                _members[member.ID] = member;
                Members.Add(member);
            }
        }
        
        private void ConsumeFetchedProjects(IList<Common.DTO.Project> fetchedProjects)
        {
            IList<Presentation.Project> projects = Mapper.Map<IList<Common.DTO.Project>, IList<Presentation.Project>>(fetchedProjects);

            this.UserProjects.Clear();

            foreach (var projectPres in projects)
            {
                this.UserProjects.Add(projectPres);

                var projectDTO = fetchedProjects.First(p => projectPres.ID.Equals(p.ID.ToString()));
                MapDependenciesInProjectsPair(projectDTO, projectPres);
                RegisterForProjectEvents(projectPres);
            }

            this.UserProjects.CollectionChanged += new NotifyCollectionChangedEventHandler(UserProjects_CollectionChanged);
        }

        public void LoginWithCredentials(string login, string password, Action<bool, string> callback)
        {
            dataProvider.fetchMemberWithLoginAndPassword(login, password, m =>
            {
                if (m == null)
                {
                    callback(false, "Wrong login and/or password");
                }
                else
                {
                    this.CurrentUserDTO = m;
                    this.CurrentUserPres = Mapper.Map<Common.DTO.Member, Presentation.Member>(m);

                    callback(true, this.CurrentUserPres.FullName);
                }
            });
        }

        public void NewMemberWithCredentials(string name, string surname, string login, string password, Action<bool, string> callback)
        {
            var member = new Common.DTO.Member(name, surname);
            member.ID = ObjectId.GenerateNewId();

            dataProvider.addNewMemberWithLoginAndPassword(member, login, password, s => {
                if (s == OperationResult.Success)
                {
                    this.CurrentUserDTO = member;
                    this.CurrentUserPres = Mapper.Map<Common.DTO.Member, Presentation.Member>(member);
                    callback(true, this.CurrentUserPres.FullName);
                }
                else if (s == OperationResult.PriviligesError)
                {
                    callback(false, "Member with this login already exists");
                }
                else
                {
                    callback(false, "Database error");
                }
            });

        }

        public void FetchStartupData(Action<bool> callback)
        {

            dataProvider.fetchAllMembersAsync((r, m) =>
                {
                    if (r == OperationResult.Success)
                    {
                        ConsumeFetchedMembers(m);

                        dataProvider.fetchProjectsForMemberAsync(CurrentUserDTO, ((s, t) =>
                        {
                            if (s == OperationResult.Success)
                            {
                                ConsumeFetchedProjects(t);

                                System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
                                dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
                                dispatcherTimer.Interval = new TimeSpan(0, 0, kPullInterval);
                                dispatcherTimer.Start();

                                callback(true);
                            }
                            else
                            {
                                ShowMessageForOperationResult(s);
                                callback(false);
                            }
                        }));
                    }
                    else
                    {
                        ShowMessageForOperationResult(r);
                        callback(false);
                    }
                });
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            var tasks = new Dictionary<ObjectId,DateTime>();
            foreach (var pair in _tasks)
	        {
                var task = pair.Value;
                tasks[new ObjectId(task.ID)] = task.Timestamp;
	        }

            dataProvider.fetchUpdatedTasksAsync(tasks, (r, t) =>
            {
                if (r == OperationResult.Success)
                {
                    foreach (var task in t)
                    {
                        RemapTask(task);
                    }
                }
                else
                {
                    ShowMessageForOperationResult(r);
                }
            });


            var projects = new Dictionary<ObjectId,DateTime>();
            foreach (var pair in _projects)
	        {
                var project = pair.Value;
                projects[new ObjectId(project.ID)] = project.Timestamp;
	        }

            dataProvider.fetchUpdatedProjectsAsync(projects, (r, t) =>
            {
                if (r == OperationResult.Success)
                {
                    foreach (var project in t)
                    {
                        RemapProject(project);
                    }
                }
                else
                {
                    ShowMessageForOperationResult(r);
                }
            });
        }

        private void ShowMessageForOperationResult(OperationResult result)
        {
            if (result != OperationResult.Success)
            {
                System.Windows.MessageBox.Show(result.ToString());
            }
        }

        #region Properties changes

        void task_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var task = sender as Presentation.Task;
            var DTOTask = Mapper.Map<Presentation.Task, Common.DTO.Task>(task);

            if (e.PropertyName.Equals("Removed"))
            {
                _tasks.Remove(task.ID);
                task.Phase.Tasks.Remove(task);
                dataProvider.removeTask(DTOTask);
            }
            else
            {
                if (e.PropertyName.Equals("Priority"))
                {
                    SortPhase(task.Phase);
                }

                dataProvider.updateTask(DTOTask, r => {
                    if (r == OperationResult.OutOfSync)
                        ReloadTaskWithID(DTOTask.ID);

                    ShowMessageForOperationResult(r);
                });
            }
        }

        void RemapTask(Common.DTO.Task task)
        {
            Presentation.Task taskPres = null;
            if (_tasks.TryGetValue(task.ID.ToString(), out taskPres))
            {
                Mapper.Map<Common.DTO.Task, Presentation.Task>(task, taskPres);
                RemapMembersInTask(task, taskPres);
            }
        }

        void RemapMembersInTask(Common.DTO.Task taskDTO, Presentation.Task taskPres)
        {
            foreach (var item in taskDTO.Members)
            {
                Presentation.Member member = null;
                if (_members.TryGetValue(item.ToString(), out member))
                {
                    taskPres.Members.Add(member);
                }
            }
        }

        void ReloadTaskWithID(ObjectId taskID)
        {
            this.dataProvider.fetchTaskWithIDAsync(taskID, (r, t) =>
            {
                if (r == OperationResult.Success)
                {
                    RemapTask(t);
                }
                else
                {
                    ShowMessageForOperationResult(r);
                }
            });
        }

        void RemapProject(Common.DTO.Project projectDTO)
        {
            shouldInformServer = false;
            Presentation.Project oldProject = this.userProjects.First(p => p.ID.Equals(projectDTO.ID.ToString()));

            UnregisterForProjectEvents(oldProject);
            Presentation.Project projectPres = Mapper.Map<Common.DTO.Project, Presentation.Project>(projectDTO, oldProject);

            MapDependenciesInProjectsPair(projectDTO, projectPres);
            RegisterForProjectEvents(projectPres);

            shouldInformServer = true;

        }

        void ReloadProjectWithID(ObjectId projectID)
        {
            this.dataProvider.fetchProjectWithIDAsync(projectID, (r, a) => {

                if (r == OperationResult.Success)
                {
                    RemapProject(a);
                }
                else
                {
                    ShowMessageForOperationResult(r);
                }
            });
        }

        void phase_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var phase = sender as Presentation.Phase;
            var DTOPhase = Mapper.Map<Presentation.Phase, Common.DTO.Phase>(phase);

            if (e.PropertyName.Equals("Removed"))
            {
                _phases.Remove(phase.ID);
                phase.Project.Phases.Remove(phase);
                dataProvider.removePhase(DTOPhase);
            }
            else if (!e.PropertyName.Equals("Tasks"))
            {
                dataProvider.updatePhase(DTOPhase, r =>
                {
                    // phase changes are last man standing...
                    if (r != OperationResult.Success)
                        ShowMessageForOperationResult(r);
                });
            }
        }


        void project_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var project = sender as Presentation.Project;
            var DTOProject = Mapper.Map<Presentation.Project, Common.DTO.Project>(project);

            if (e.PropertyName.Equals("Removed"))
            {
                _projects.Remove(project.ID);
                userProjects.Remove(project);
                dataProvider.removeProject(DTOProject);
            }
            else
            {
                dataProvider.updateProject(DTOProject, r =>
                {
                    if (r == OperationResult.OutOfSync)
                        ReloadProjectWithID(DTOProject.ID);

                    ShowMessageForOperationResult(r);
                });
            }
        }

#endregion

        #region Collections changes

        private bool shouldInformServer = true;

        void Tasks_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

           
            var collection = sender as OwnedObservableCollection<Presentation.Task>;
            var phase = collection.Owner as Presentation.Phase;
            var DTOPhase = Mapper.Map<Presentation.Phase, Common.DTO.Phase>(phase);

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Presentation.Task task in e.NewItems)
                {
                    task.Phase = phase;

                    if (task.ID == null)
                    {
                        task.ID = ObjectId.GenerateNewId().ToString();
                        var DTOTask = Mapper.Map<Presentation.Task, Common.DTO.Task>(task);

                        RegisterForTaskEvents(task);

                        if (shouldInformServer)
                        {
                            dataProvider.addNewTaskToPhase(DTOTask, DTOPhase);
                        }
                    }
                    else
                    {
                        var DTOTask = Mapper.Map<Presentation.Task, Common.DTO.Task>(task);

                        if (shouldInformServer)
                        {
                            dataProvider.updateTask(DTOTask, r =>
                            {
                                if (r == OperationResult.OutOfSync)
                                    ReloadTaskWithID(DTOTask.ID);

                                ShowMessageForOperationResult(r);
                            });

                            dataProvider.moveTaskToPhase(DTOTask, DTOPhase);
                        }
                    }
                }
            }

            SortPhase(phase);
        }


        void TaskMembers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!shouldInformServer) return;

            var collection = sender as OwnedObservableCollection<Presentation.Member>;

            var task = (collection).Owner as Presentation.Task;
            var DTOTask = Mapper.Map<Presentation.Task, Common.DTO.Task>(task);

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Presentation.Member member in e.NewItems)
                {
                    var DTOMember = Mapper.Map<Presentation.Member, Common.DTO.Member>(member);
                    dataProvider.addMemberToTask(DTOMember, DTOTask);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (Presentation.Member member in e.OldItems)
                {
                    var DTOMember = Mapper.Map<Presentation.Member, Common.DTO.Member>(member);
                    dataProvider.removeMemberFromTask(DTOMember, DTOTask);
                }
            }
        }


        void ProjectMembers_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!shouldInformServer) return;

            var collection = sender as OwnedObservableCollection<Presentation.Member>;
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var project = (collection).Owner as Presentation.Project;
                var DTOProject = Mapper.Map<Presentation.Project, Common.DTO.Project>(project);

                foreach (Presentation.Member member in e.NewItems)
                {
                    var DTOMember = Mapper.Map<Presentation.Member, Common.DTO.Member>(member);
                    dataProvider.addMemberToProject(DTOMember, DTOProject);
                }
            }
        }

        void Phases_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!shouldInformServer) return;

            var collection = sender as OwnedObservableCollection<Presentation.Phase>;
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var project = (collection).Owner as Presentation.Project;
                var DTOProject = Mapper.Map<Presentation.Project, Common.DTO.Project>(project);

                foreach (Presentation.Phase phase in e.NewItems)
                {
                    if (phase.ID == null)
                    {
                        phase.ID = ObjectId.GenerateNewId().ToString();
                        phase.PropertyChanged += new PropertyChangedEventHandler(phase_PropertyChanged);
                        phase.Tasks.CollectionChanged += new NotifyCollectionChangedEventHandler(Tasks_CollectionChanged);
                        _phases[phase.ID] = phase;
                    }
                    phase.Project = project;

                    var DTOPhase = Mapper.Map<Presentation.Phase, Common.DTO.Phase>(phase);
                    dataProvider.addNewPhaseToProject(DTOPhase, DTOProject);
                }
            }
        }

        void UserProjects_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (!shouldInformServer) return;

            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (Presentation.Project project in e.NewItems)
                {
                    if (project.ID == null)
                    {
                        project.ID = ObjectId.GenerateNewId().ToString();
                        project.Members.Add(CurrentUserPres);

                        RegisterForProjectEvents(project);
                    }
                    var DTOProject = Mapper.Map<Presentation.Project, Common.DTO.Project>(project);
                    dataProvider.addNewProjectForMember(DTOProject, CurrentUserDTO);
                }
            }
        }
        #endregion

        void SortPhase(Presentation.Phase phase)
        {
            shouldInformServer = false;
            phase.Tasks = new OwnedObservableCollection<Presentation.Task>(phase.Tasks.OrderByDescending(i => i.Priority));
            phase.Tasks.CollectionChanged += new NotifyCollectionChangedEventHandler(Tasks_CollectionChanged);
            phase.Tasks.Owner = phase;
            shouldInformServer = true;
            
        }
    }


}
