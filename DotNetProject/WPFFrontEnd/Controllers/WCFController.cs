﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DTO;
using Common.OperationResult;

namespace WPFFrontEnd.Controllers
{
    class WCFController : IServerDataAccess
    {
        private ServiceReference.DataAccessClient dataProvider;

        public WCFController()
        {
            dataProvider = new ServiceReference.DataAccessClient();
        }

        public async void fetchMemberWithLoginAndPassword(string login, string password, Action<Member> callback)
        {
            dataProvider = new ServiceReference.DataAccessClient();
            dataProvider.ClientCredentials.UserName.Password = password;
            dataProvider.ClientCredentials.UserName.UserName = login;

            Member result;
            try
            {
                result = await dataProvider.memberWithLoginAndPasswordAsync(login, password);

            }
            catch (Exception)
            {
                result = null;
            }
            callback(result);
        }
        public async void addNewMemberWithLoginAndPassword(Member newMember, string login, string password, Action<OperationResult> callback)
        {
            var newUserProvider = new ServiceReference1.NewMemberClient();
            dataProvider.ClientCredentials.UserName.Password = password;
            dataProvider.ClientCredentials.UserName.UserName = login;
            var result = await newUserProvider.saveNewMemberWithLoginAndPasswordAsync(newMember, login, password);
            callback(result);
        }


        public async void fetchAllMembersAsync(Action<OperationResult, IList<Member>> callback)
        {
                var result = await dataProvider.allMembersAsync();
                callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);

        }

        public async void fetchMemberWithIDAsync(MongoDB.Bson.ObjectId id, Action<OperationResult, Common.DTO.Member> callback)
        {
            var result = await dataProvider.memberWithIDAsync(id);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchMembersWithIDsAsync(IList<MongoDB.Bson.ObjectId> id, Action<OperationResult, IList<Common.DTO.Member>> callback)
        {
            var result = await dataProvider.membersWithIDsAsync(id.ToArray());
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }



        public async void fetchTasksForMemeberAsync(Member member, Action<OperationResult, IList<Common.DTO.Task>> callback)
        {
            var result = await dataProvider.tasksForMemberAsync(member);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }


        public async void fetchTaskWithIDAsync(MongoDB.Bson.ObjectId id, Action<OperationResult, Common.DTO.Task> callback)
        {
            var result = await dataProvider.taskWithIDAsync(id);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchTasksWithIDsAsync(IList<MongoDB.Bson.ObjectId> ids, Action<OperationResult, IList<Common.DTO.Task>> callback)
        {
            var result = await dataProvider.tasksWithIDsAsync(ids.ToArray());
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchUpdatedTasksAsync(Dictionary<MongoDB.Bson.ObjectId, DateTime> clientsTimestamps, Action<OperationResult, IList<Common.DTO.Task>> callback)
        {
            var result = await dataProvider.updatedTasksAsync(clientsTimestamps);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchProjectsForMemberAsync(Member member, Action<OperationResult, IList<Project>> callback)
        {
            var result = await dataProvider.projectsForMemberAsync(member);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchProjectWithIDAsync(MongoDB.Bson.ObjectId id, Action<OperationResult, Project> callback)
        {
            var result = await dataProvider.projectWithIDAsync(id);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchProjectsWithIDsAsync(IList<MongoDB.Bson.ObjectId> id, Action<OperationResult, IList<Project>> callback)
        {
            var result = await dataProvider.projectsWithIDsAsync(id.ToArray());
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void fetchUpdatedProjectsAsync(Dictionary<MongoDB.Bson.ObjectId, DateTime> clientsTimestamps, Action<OperationResult, IList<Project>> callback)
        {
            var result = await dataProvider.updatedProjectsAsync(clientsTimestamps);
            callback(result != null ? OperationResult.Success : OperationResult.DatabaseError, result);
        }

        public async void addMemberToProject(Member member, Project project)
        {
            var result = await dataProvider.addMemberToProjectAsync(member, project);
        }

        public async void removeMemberFromProject(Member member, Project project)
        {
            var result = await dataProvider.removeMemberFromProjectAsync(member, project);
        }

        public async void addMemberToTask(Member member, Common.DTO.Task task)
        {
            var result = await dataProvider.addMemberToTaskAsync(member, task);
        }

        public async void removeMemberFromTask(Member member, Common.DTO.Task task)
        {
            var result = await dataProvider.removeMemberFromTaskAsync(member, task);
        }

        public async void addNewTaskToPhase(Common.DTO.Task newTask, Phase targetPhase)
        {
            var result = await dataProvider.addNewTaskToPhaseAsync(newTask, targetPhase);
        }

        public async void updateTask(Common.DTO.Task updatedTask, Action<OperationResult> callback)
        {
            var result = await dataProvider.updateTaskAsync(updatedTask);
            callback(result);
        }

        public async void removeTask(Common.DTO.Task removedTask)
        {
            var result = await dataProvider.removeTaskAsync(removedTask);
        }

        public async void moveTaskToPhase(Common.DTO.Task movedTask, Phase targetPhase)
        {
            var result = await dataProvider.moveTaskToPhaseAsync(movedTask, targetPhase);
        }

        public async void addNewPhaseToProject(Phase newPhase, Project targetProject)
        {
            var result = await dataProvider.addNewPhaseToProjectAsync(newPhase, targetProject);
        }

        public async void updatePhase(Phase updatedPhase, Action<OperationResult> callback)
        {
            var result = await dataProvider.updatePhaseAsync(updatedPhase);
            callback(result);
        }

        public async void removePhase(Phase removedPhase)
        {
            var result = await dataProvider.removePhaseAsync(removedPhase);

        }

        public async void addNewProjectForMember(Project newProject, Member targetMember)
        {
            var result = await dataProvider.addNewProjectForMemberAsync(newProject, targetMember);
        }

        public async void updateProject(Project updatedProject, Action<OperationResult> callback)
        {
            var result = await dataProvider.updateProjectAsync(updatedProject);
            callback(result);
        }

        public async void removeProject(Project removedProject)
        {
            var result = await dataProvider.removeProjectAsync(removedProject);
        }

    }

}
