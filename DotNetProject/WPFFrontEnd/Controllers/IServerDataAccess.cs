﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common.DTO;
using Common.OperationResult;

namespace WPFFrontEnd.Controllers
{
    interface IServerDataAccess
    {
        void fetchMemberWithLoginAndPassword(string login, string password, Action<Member> callback);
        void addNewMemberWithLoginAndPassword(Member newMember, string login, string password, Action<OperationResult> callback);

        void fetchTasksForMemeberAsync(Member member, Action<OperationResult, IList<Common.DTO.Task>> callback);
        void fetchTaskWithIDAsync(MongoDB.Bson.ObjectId id, Action<OperationResult, Common.DTO.Task> callback);
        void fetchTasksWithIDsAsync(IList<MongoDB.Bson.ObjectId> id, Action<OperationResult, IList<Common.DTO.Task>> callback);
        void fetchUpdatedTasksAsync(Dictionary<MongoDB.Bson.ObjectId, System.DateTime> clientsTimestamps, Action<OperationResult, IList<Common.DTO.Task>> callback);

        void fetchProjectsForMemberAsync(Member member, Action<OperationResult, IList<Project>> callback);
        void fetchProjectWithIDAsync(MongoDB.Bson.ObjectId id, Action<OperationResult, Project> callback);
        void fetchProjectsWithIDsAsync(IList<MongoDB.Bson.ObjectId> id, Action<OperationResult, IList<Project>> callback);
        void fetchUpdatedProjectsAsync(Dictionary<MongoDB.Bson.ObjectId, System.DateTime> clientsTimestamps, Action<OperationResult, IList<Project>> callback);

        void fetchAllMembersAsync(Action<OperationResult, IList<Member>> callback);
        void fetchMemberWithIDAsync(MongoDB.Bson.ObjectId id, Action<OperationResult, Common.DTO.Member> callback);
        void fetchMembersWithIDsAsync(IList<MongoDB.Bson.ObjectId> id, Action<OperationResult, IList<Common.DTO.Member>> callback);

        // CRUD
        void addMemberToProject(Member member, Project project);
        void removeMemberFromProject(Member member, Project project);

        void addMemberToTask(Member member, Common.DTO.Task task);
        void removeMemberFromTask(Member member, Common.DTO.Task task);

        void addNewTaskToPhase(Common.DTO.Task newTask, Phase targetPhase);
        void updateTask(Common.DTO.Task updatedTask, Action<OperationResult> callback); //called on name, description, order, etc. change
        void removeTask(Common.DTO.Task removedTask);
        void moveTaskToPhase(Common.DTO.Task movedTask, Phase targetPhase);

        void addNewPhaseToProject(Phase newPhase, Project targetProject);
        void updatePhase(Phase updatedPhase, Action<OperationResult> callback); //called on properties change
        void removePhase(Phase removedPhase);

        void addNewProjectForMember(Project newProject, Member targetMember);
        void updateProject(Project updatedProject, Action<OperationResult> callback); //called on properties change
        void removeProject(Project removedProject);
    }
}
