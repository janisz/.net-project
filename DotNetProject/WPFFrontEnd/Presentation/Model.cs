﻿
using System.ComponentModel;

namespace WPFFrontEnd.Presentation
{
    /// <summary>
    /// Abstract class containing shared properties of its subclasses
    /// </summary>
    abstract class Model : INotifyPropertyChanged
    {
        public Model(string name)
        {
            this.name = name;
            this.Timestamp = System.DateTime.MinValue;
        }

        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        public System.DateTime Timestamp
        {
            get;
            set;
        }


        private bool removed = false;

        public bool Removed
        {
            get { return removed; }
            set
            {
                if (value != removed)
                {
                    removed = value;
                    OnPropertyChanged("Removed");
                }
            }
        }

        private string id;

        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
