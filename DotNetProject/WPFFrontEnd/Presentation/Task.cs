﻿using WPFFrontEnd.Misc;

namespace WPFFrontEnd.Presentation
{
    /// <summary>
    /// Represents a single task in the Presentation layer
    /// </summary>
    class Task : Model
    {
        public Task(string name) : base(name)
        {
            members = new OwnedObservableCollection<Member>() { Owner = this };
        }


        public Phase Phase
        {
            get;
            set;
        }

        private OwnedObservableCollection<Member> members;

        public OwnedObservableCollection<Member> Members
        {
            get { return members; }
            set { members = value; }
        }

        int priority;
        public int Priority
        {
            get { return priority; }
            set
            {
                if (value != priority && priority >= 0 && priority < 5)
                {
                    priority = value;
                    OnPropertyChanged("Priority");
                }
            }
        }

        string description;
        public string Description
        {
            get { return description; }
            set
            {
                if (value != description)
                {
                    description = value;
                    OnPropertyChanged("Description");
                }
            }
        }

    }
}
