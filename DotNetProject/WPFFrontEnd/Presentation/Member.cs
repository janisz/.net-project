﻿
namespace WPFFrontEnd.Presentation
{
    /// <summary>
    /// Represents a single member of a Task or Project in the Presentation layer
    /// </summary>
    class Member : Model
    {

        public Member(string name)
            : base(name)
        {
        }

        private string surname;

        public string Surname
        {
            get { return surname; }
            set
            {
                if (value != surname)
                {
                    surname = value;
                    OnPropertyChanged("Surname");
                }
            }
        }

        public string FullName
        {
            get { return Name + " " + Surname; }
        }
    }
}
