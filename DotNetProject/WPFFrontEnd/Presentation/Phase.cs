﻿
using WPFFrontEnd.Misc;

namespace WPFFrontEnd.Presentation
{
    /// <summary>
    /// Represents a single Phase of a Project in the Presentation layer
    /// </summary>
    class Phase : Model
    {
        public Phase(string name) : base(name)
        {
            tasks = new OwnedObservableCollection<Task>() { Owner = this };
        }

        public Project Project
        {
            get;
            set;
        }


        private OwnedObservableCollection<Task> tasks;

        public OwnedObservableCollection<Task> Tasks
        {
            get { return tasks; }
            set
            {
                if (value != tasks)
                {
                    tasks = value;
                    OnPropertyChanged("Tasks");
                }
            }
        }
    }
}
