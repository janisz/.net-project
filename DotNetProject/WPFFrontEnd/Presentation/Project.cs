﻿
using WPFFrontEnd.Misc;

namespace WPFFrontEnd.Presentation
{
    /// <summary>
    /// Represents a Project in the Presentation layer
    /// </summary>
    class Project : Model
    {
        public Project(string name) : base(name)
        {
            phases = new OwnedObservableCollection<Phase>() { Owner = this };
            members = new OwnedObservableCollection<Member>() { Owner = this };
        }

        private OwnedObservableCollection<Member> members;

        public OwnedObservableCollection<Member> Members
        {
            get { return members; }
            set { members = value; }
        }


        private OwnedObservableCollection<Phase> phases;

        public OwnedObservableCollection<Phase> Phases
        {
            get { return phases; }
            set { phases = value; }
        }
    }
}
