﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.DTO;
using Server;
using MongoDB.Bson;

namespace WebFrontEnd.Controllers
{
    public class HomeController : Controller
    {
        MongoDataAccess dataAccess = new MongoDataAccess();

        [Authorize]     
        public ActionResult Index()
        {
            if (Session["memberId"] == null)
                return Redirect("~/Account/Login");
            var member = dataAccess.memberWithID(new ObjectId(Session["memberId"].ToString()));
            ViewBag.Member = member;
            var projects = dataAccess.projectsForMember(member);
            ViewBag.Projects = projects;
            ViewBag.Project = null;
            if (Request.Params["project"] != null)
                ViewBag.Project = projects.FirstOrDefault(p => p.ID == new ObjectId(Request.Params["project"]));
            
            ViewBag.DataAccess = dataAccess;
            ViewBag.PriorityDictonary = ConvertColor();
            Session["ref"] = Request.ServerVariables["HTTP_REFERER"];
            return View();
        }

        /// <summary>
        /// Converts numeric priority value to it's color representation
        /// </summary>
        public Dictionary<int, string> ConvertColor()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();

            dict.Add(0,
                  "rgb(225, 225, 225)");

            dict.Add(1,
                 "rgb(255, 247, 153)");

            dict.Add(2,
                 "rgb(253, 198, 137)");

            dict.Add(3,
                "rgb(247, 148, 29)");

            dict.Add(4,
                "rgb(255, 0, 0)");

            return dict;
        }

        [Authorize]     
        public ActionResult Task()
        {
            ViewBag.TaskId = Request.Params["taskId"];
            ViewBag.Task = dataAccess.taskWithID(ViewBag.TaskId);
            if (Request.Params["delete"] != null && ViewBag.TaskId != null)
            {
                dataAccess.removeTask(ViewBag.Task);
                return Redirect("/");
            }
            Session["ref"] = Request.ServerVariables["HTTP_REFERER"];
            return View();
        }

        [HttpPost]
        [Authorize]     
        public ActionResult Task(string Name, string Description, string taskId, int Priority)
        {
            var task = dataAccess.taskWithID(new MongoDB.Bson.ObjectId(taskId));
            if (task != null)
            {
                task.Name = Name;
                task.Description = Description;
                task.Priority = Priority;
                dataAccess.updateTask(task);
            }
            return Redirect((string)Session["ref"] ?? "/");
        }

        [Authorize]     
        public ActionResult NewTask()
        {
            ViewBag.PhaseId = Request.Params["phase"];
            Session["ref"] = Request.ServerVariables["HTTP_REFERER"];
            return View();
        }

        [HttpPost]
        [Authorize]     
        public ActionResult NewTask(string Name, string Description, string phaseId, int Priority)
        {
            var task = new Task(Name);           
            task.Description = Description;
            task.Priority = Priority;
            var phase = new Phase("");
            phase.ID = new MongoDB.Bson.ObjectId(phaseId);
            dataAccess.addNewTaskToPhase(task, phase );
            return Redirect((string)Session["ref"] ?? "/" );
        }

        [Authorize]     
        public ActionResult Phase()
        {
            ViewBag.ProjectId = Request.Params["project"];
            ViewBag.PhaseId = Request.Params["phase"];
            if (Request.Params["delete"] != null && ViewBag.PhaseId != null)
            {
                var phase = new Phase("");
                phase.ID = new MongoDB.Bson.ObjectId(ViewBag.PhaseId);
                dataAccess.removePhase(phase);
                return Redirect((string)Session["ref"] ?? "/");  
            }
            Session["ref"] = Request.ServerVariables["HTTP_REFERER"];
            return View();
        }

        [HttpPost]
        [Authorize]     
        public ActionResult Phase(string Name, string projectId, string phaseId)
        {
            var phase = new Phase(Name);           
            if (phaseId != null && phaseId != "")
            {
                phase.ID = new MongoDB.Bson.ObjectId(phaseId);               
                dataAccess.updatePhase(phase);
            }
            else
            {
                var proj = new Project("");
                proj.ID = new MongoDB.Bson.ObjectId(projectId);
                dataAccess.addNewPhaseToProject(phase, proj);
            }

            return Redirect((string)Session["ref"] ?? "/");
        }

        [Authorize]     
        public ActionResult Project()
        {
            ViewBag.ProjectId = Request.Params["project"];
            ViewBag.MemberId = Session["memberId"];
            if (Request.Params["delete"] != null && ViewBag.ProjectId != null)
            {
                var proj = dataAccess.projectWithID(new MongoDB.Bson.ObjectId(ViewBag.ProjectId));               
                dataAccess.removeProject(proj);
                return Redirect("/");
            }
            Session["ref"] = Request.ServerVariables["HTTP_REFERER"];
            return View();
        }

        [HttpPost]
        [Authorize]     
        public ActionResult Project(string Name, string memberId, string projectId)
        {
            var proj = new Project(Name);
            if (projectId != null && projectId != "")
            {
                proj = dataAccess.projectWithID(new MongoDB.Bson.ObjectId(projectId));
                proj.Name = Name;
                dataAccess.updateProject(proj);
            }
            else
            {
                var member = new Member("", ""); //FIXME get member from cookie
                member.ID = new MongoDB.Bson.ObjectId(memberId);
                dataAccess.addNewProjectForMember(proj, member); 
            }

            return Redirect((string)Session["ref"] ?? "/");
        }

        [HttpPost]
        [HandleError]
        [Authorize]     
        public ActionResult MoveTask(string taskId, string phaseId)
        {
            var task = dataAccess.taskWithID(new ObjectId(taskId));
            var phase = new Phase("");
            phase.ID = new ObjectId(phaseId);
            var result = dataAccess.moveTaskToPhase(task, phase);
            if (result == Common.OperationResult.OperationResult.Success)
                return Json("OK");

            return new HttpStatusCodeResult(409, "Error" + result);
        }

        [Authorize]     
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            return View();
        }

        [Authorize]     
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
