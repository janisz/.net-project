﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;

namespace Server
{
    public class DistributorValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                throw new SecurityTokenException("Username and password required");
            MongoDataAccess da = new MongoDataAccess();
            var m = da.memberWithLoginAndPassword(userName, password);
            if (m == null)
                throw new FaultException(string.Format("Wrong username ({0}) or password ", userName));   
        }
    }
}
