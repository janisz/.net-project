﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Common.OperationResult;
using Common.DTO;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INewMember" in both code and config file together.
    [ServiceContract]
    public interface INewMember
    {
        [OperationContract]
        OperationResult saveNewMemberWithLoginAndPassword(Member member, string login, string password);
    }
}
