﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Common.DTO;
using Common.OperationResult;

namespace Server
{

    [ServiceContract]
    public interface IDataAccess
    {
        [OperationContract]
        void SetDbName(string dbName);
        [OperationContract]
        string GetDbName();
        [OperationContract]
        void SetConnectionString(string connectionString);
        [OperationContract]
        string GetConnectionString();

        [OperationContract]
        IList<Task> tasksForMember(Member member); // this will be the first one called
        [OperationContract]
        IList<Project> projectsForMember(Member member);

        [OperationContract]
        Member memberWithLoginAndPassword(string login, string password);
        
        [OperationContract]
        IList<Member> allMembers(); // return all members from database
        [OperationContract]
        Member memberWithID(MongoDB.Bson.ObjectId id);
        [OperationContract]
        IList<Member> membersWithIDs(IList<MongoDB.Bson.ObjectId> ids);


        [OperationContract]
        Task taskWithID(MongoDB.Bson.ObjectId id);
        [OperationContract]
        IList<Task> tasksWithIDs(IList<MongoDB.Bson.ObjectId> ids);
        [OperationContract]
        IList<Task> updatedTasks(Dictionary<MongoDB.Bson.ObjectId, System.DateTime> clientsTimestamps);
        //returns only those tasks that have newer timestamp than the one provided by client

        [OperationContract]
        OperationResult addMemberToProject(Member member, Project project);
        [OperationContract]
        OperationResult removeMemberFromProject(Member member, Project project);

        [OperationContract]
        OperationResult addMemberToTask(Member member, Task task);
        [OperationContract]
        OperationResult removeMemberFromTask(Member member, Task task);

        [OperationContract]
        OperationResult addNewTaskToPhase(Task newTask, Phase targetPhase);
        [OperationContract]
        OperationResult updateTask(Task updatedTask); //called on name, description, order, etc. change
        [OperationContract]
        OperationResult removeTask(Task removedTask);
        [OperationContract]
        OperationResult moveTaskToPhase(Task movedTask, Phase targetPhase);

        [OperationContract]
        OperationResult addNewPhaseToProject(Phase newPhase, Project targetProject);
        [OperationContract]
        OperationResult updatePhase(Phase updatedPhase); //called on properties change
        [OperationContract]
        OperationResult removePhase(Phase removedPhase);

        [OperationContract]
        OperationResult addNewProjectForMember(Project newProject, Member targetMember);
        [OperationContract]
        OperationResult updateProject(Project updatedProject);
        [OperationContract]
        OperationResult removeProject(Project removedProject);


        [OperationContract]
        Project projectWithID(MongoDB.Bson.ObjectId id);
        [OperationContract]
        IList<Project> projectsWithIDs(IList<MongoDB.Bson.ObjectId> ids);
        [OperationContract]
        IList<Project> updatedProjects(Dictionary<MongoDB.Bson.ObjectId, System.DateTime> clientsTimestamps);
        //returns only those tasks that have newer timestamp than the one provided by client

    }

}
