﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.DTO;
using Common.OperationResult;
using System.ServiceModel;
using MongoDB.Driver.Linq;
using Common;


namespace Server
{
    /// <summary>
    /// This class is effectively a DAL. It's responsible for setting and getting information from/to MongoDB.
    /// </summary>
    public class MongoDataAccess : IDataAccess
    {
        #region Privates
        MongoServer     server;
        MongoDatabase   db;

        string dbName;
        string connectionString;

        MongoCollection<Member> Members;
        MongoCollection<Project> Projects;
        MongoCollection<Task> Tasks;
        MongoCollection<Credentials> Credentials;

        private void EstabilishConnection()
        {
            server = MongoServer.Create(connectionString);
            db = server.GetDatabase(dbName);

            Members = db.GetCollection<Member>("members");
            Projects = db.GetCollection<Project>("projects");
            Tasks = db.GetCollection<Task>("tasks");
            Credentials = db.GetCollection<Credentials>("credentials");
            Logger.Write.Debug("Connection estabilished");

            //FIXME
            var m = new Member("test", "");
            m.ID = new ObjectId();
            saveNewMemberWithLoginAndPassword(m, "test", PasswordHash.CreateHash("test"));
        }

        private Project FindProject(IMongoQuery query)
        {
            var project = Projects.FindOne(query);
            if (project == null)
            {
                Logger.Write.Error("Project not found");
            }
            return project;
        }

        private Project FindProjectById(ObjectId Id)
        {
            return FindProject(Query.EQ("_id", Id));
        }

        private OperationResult SaveProjectForce(Project project, bool force)
        {
            var projectFromDb = Projects.FindOneById(project.ID);

            if (!force && projectFromDb != null && project.Timestamp < projectFromDb.Timestamp)
            {
                Logger.Write.Debug("Outdated project, need to update data");
                return OperationResult.OutOfSync;
            }

            try
            {
                project.Timestamp = DateTime.Now;
                Projects.Save(project);
                Logger.Write.Debug("Save Project: " + project.ID);
            }
            catch (MongoException me)
            {
                Logger.Write.Error(me.Message);
                return OperationResult.DatabaseError;
            }

            return OperationResult.Success;
        }
        private OperationResult SaveProject(Project project)
        {
            return SaveProjectForce(project, false);
        }

        private OperationResult SaveMember(Member member)
        {
            var oldMember = Members.FindOneById(member.ID);
            if (oldMember != null && oldMember.Timestamp > member.Timestamp)
            {
                 Logger.Write.Debug("Outdated member, need to update data");
                 return OperationResult.OutOfSync;
            }

            try
            {
                member.Timestamp = DateTime.Now;
                Members.Save(member);
                Logger.Write.Debug("Save Member: " + member.ID);
            }
            catch (MongoException me)
            {
                Logger.Write.Error(me.Message);
                return OperationResult.DatabaseError;
            }
            
            return OperationResult.Success;
        }

        private OperationResult SaveTaskForce(Task task, bool force)
        {
            var oldTask = Tasks.FindOneById(task.ID);
            
            if (!force && oldTask != null && oldTask.Timestamp > task.Timestamp)
            {
                Logger.Write.Debug("Outdated proj, need to update data");
                return OperationResult.OutOfSync;
            }
            
            try
            {
                task.Timestamp = DateTime.Now;
                Tasks.Save(task);
                Logger.Write.Debug("Save proj: " + task.ID);
            }
            catch (MongoException me)
            {
                Logger.Write.Error(me.Message);
                return OperationResult.DatabaseError;
            }

            return OperationResult.Success;
        }

        private OperationResult SaveTask(Task task)
        {
            return SaveTaskForce(task, false);
        }

        private Task FindTaskById(ObjectId id)
        {
            return Tasks.FindOneById(id);
        }

        private Task FindTaskById(string id)
        {
            return Tasks.FindOneById(new ObjectId(id));
        }

        private Member FindMemberById(ObjectId id)
        {
            return Members.FindOneById(id);
        }

        #endregion

        public MongoDataAccess()
        {
            this.dbName = "dotNet";
            this.connectionString = "mongodb://localhost";
            EstabilishConnection();
        }

        public void DropDatabase()
        {
            db.Drop();
        }

        #region Interface implementation

        public void SetDbName(string databaseName)
        {
            this.dbName = databaseName;
            EstabilishConnection();
        }

        public void SetConnectionString(string connectionString)
        {
            this.connectionString = connectionString;
            EstabilishConnection();
        }

        public string GetDbName()
        {
            return dbName;
        }

        public string GetConnectionString()
        {
            return connectionString;
        }

        /// <summary>
        /// Return all tasks for specified member
        /// </summary>
        /// <param name="member"> only member.ID will be used</param>
        /// <returns></returns>
        public IList<Task> tasksForMember(Member member)
        {
            var array = new List<ObjectId>() { member.ID };
            var query = Query.All("Members", BsonArray.Create(array));
            return Tasks.Find(query).SetSortOrder(SortBy.Ascending("_id")).ToList();
        }

        /// <summary>
        /// Return all projects that involves member. 
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public IList<Project> projectsForMember(Member member)
        {
            var array = new List<ObjectId>() { member.ID };
            var query = Query.All("Members", BsonArray.Create(array));
            var proj = Projects.Find(query).SetSortOrder(SortBy.Ascending("_id")).ToList();
            return proj;
        }

        /// <summary>
        /// Return list of all members from batabase
        /// </summary>
        /// <returns></returns>
        public IList<Member> allMembers()
        {
            var membersList = Members.FindAll().ToList();
            return membersList;
        }

        public Member memberWithID(MongoDB.Bson.ObjectId id)
        {
            return Members.FindOneById(id);
        }

        public IList<Member> membersWithIDs(IList<MongoDB.Bson.ObjectId> ids)
        {
            var result = new List<Member>();
            foreach (var id in ids)
            {
                result.Add(Members.FindOneById(id));
            }
            return result;
        }

        /// <summary>
        /// Return proj with specified ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>return null if proj doesn'proj exist</returns>
        public Task taskWithID(ObjectId id)
        {
            return Tasks.FindOneById(id);
        }

        public Task taskWithID(string id)
        {
            return taskWithID( new ObjectId(id));
        }

        /// <summary>
        /// Return List of proj whith ID from ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IList<Task> tasksWithIDs(IList<ObjectId> ids)
        {
            var result = new List<Task>();
            foreach (var id in ids)
            {
                var task = Tasks.FindOneById(id);
                if (task == null) continue;

                result.Add(task);
            }
            return result;
        }

        /// <summary>
        /// Return List of tasks that have been updatede or created after time specified in timestamp
        /// </summary>
        /// <param name="clientsTimestamps"></param>
        /// <returns></returns>
        public IList<Task> updatedTasks(Dictionary<ObjectId, DateTime> clientsTimestamps)
        {
            var newerTasks = new List<Task>();
            foreach (var t in clientsTimestamps.Keys)
            {
                var task = FindTaskById(t);
                if (task == null) continue;

                if (clientsTimestamps[t] < task.Timestamp)
                {
                    newerTasks.Add(task);
                }
            }

            return newerTasks;
        }

        /// <summary>
        /// Add existing member to members list in specified project
        /// </summary>
        /// <param name="member"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public OperationResult addMemberToProject(Member member, Project project)
        {
            var proj = FindProjectById(project.ID);
            if (proj == null) return OperationResult.OutOfSync;

            proj.Timestamp = project.Timestamp;
            proj.Members.Add(member.ID);
            return SaveProjectForce(proj, true);
        }

        /// <summary>
        /// Remove member from projects' members list
        /// </summary>
        /// <param name="member"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        public OperationResult removeMemberFromProject(Member member, Project project)
        {
            var proj = FindProjectById(project.ID);
            if (proj == null) return OperationResult.OutOfSync;

            proj.Timestamp = project.Timestamp;
            proj.Members.RemoveAll(_ => _ == member.ID);
            return SaveProjectForce(proj, true);
        }

        /// <summary>
        /// Add member ID to specified proj
        /// </summary>
        /// <param name="member"></param>
        /// <param name="proj"></param>
        /// <returns></returns>
        public OperationResult addMemberToTask(Member member, Task task)
        {
            var t = Tasks.FindOneById(task.ID);

            if (t == null) return OperationResult.OutOfSync;

            t.Timestamp = task.Timestamp;

            if (t.Members.Any(p => p == member.ID))
            {
                return OperationResult.OutOfSync;
            }

            t.Members.Add(member.ID);
            return SaveTaskForce(t, true);
        }

        /// <summary>
        /// Deletes Member from proj
        /// </summary>
        /// <param name="member"></param>
        /// <param name="proj"></param>
        /// <returns></returns>
        public OperationResult removeMemberFromTask(Member member, Task task)
        {
            var t = Tasks.FindOneById(task.ID);
            if (t == null) return OperationResult.OutOfSync;

            t.ID = task.ID;
            t.Members.Remove(member.ID);
            return SaveTaskForce(t, true);
        }

        /// <summary>
        /// Add new Task.ID to specified phase
        /// </summary>
        /// <param name="newTask"></param>
        /// <param name="targetPhase"></param>
        /// <returns></returns>
        public OperationResult addNewTaskToPhase(Task newTask, Phase targetPhase)
        {
            SaveTask(newTask);
            var proj = FindProject(Query.EQ("Phases._id", targetPhase.ID));

            if (proj == null)
                return OperationResult.OutOfSync;

            var i = proj.Phases.FindIndex(_ => _.ID == targetPhase.ID);
            proj.Phases[i].TaskIDs.Add(newTask.ID);
            return SaveProject(proj);
        }

        /// <summary>
        /// Updated specified proj.
        /// </summary>
        /// <param name="updatedTask"></param>
        /// <returns></returns>
        public OperationResult updateTask(Task updatedTask)
        {
            var t = Tasks.FindOneById(updatedTask.ID);
            if (t == null) return OperationResult.OutOfSync;

            t.Name = updatedTask.Name;
            t.Description = updatedTask.Description;
            t.Priority = updatedTask.Priority;
            return SaveTask(t);
        }

        /// <summary>
        /// Remove all occurences of proj
        /// </summary>
        /// <param name="removedTask"></param>
        /// <returns></returns>
        public OperationResult removeTask(Task removedTask)
        {
            OperationResult result = OperationResult.DatabaseError;
            try
            {
                var proj = FindProject(Query.EQ("Phases.TaskIDs", removedTask.ID));

                if (proj == null)
                    return OperationResult.OutOfSync;

                //if (proj.Timestamp > removedTask.Timestamp)
                //    return OperationResult.OutOfSync;
                
                for (int i = 0; i < proj.Phases.Count; i++)
                {
                    proj.Phases[i].TaskIDs.RemoveAll(_ => _ == removedTask.ID);
                }

                Tasks.Remove(Query.EQ("_id", removedTask.ID));
                result = SaveProject(proj);
            }
            catch (MongoException me)
            {
                Logger.Write.Error(me.Message);
                return OperationResult.DatabaseError;
            }
            return result;
        }

        /// <summary>
        /// Move proj from one phase to another
        /// </summary>
        /// <param name="movedTask"></param>
        /// <param name="targetPhase"></param>
        /// <returns></returns>
        public OperationResult moveTaskToPhase(Task movedTask, Phase targetPhase)
        {
            if (movedTask == null)
            return OperationResult.OutOfSync;

            var proj = FindProject(Query.EQ("Phases.TaskIDs", movedTask.ID));

            if (proj == null)
                return OperationResult.OutOfSync;

            for (int i=0;i<proj.Phases.Count;i++)
            {
                proj.Phases[i].TaskIDs.RemoveAll(_ => _ == movedTask.ID);
            }

            int j = proj.Phases.FindIndex(_ => _.ID == targetPhase.ID);
            proj.Phases[j].TaskIDs.Add(movedTask.ID);
            return SaveProject(proj);
        }

        /// <summary>
        /// Add new phase to project
        /// </summary>
        /// <param name="newPhase"></param>
        /// <param name="targetProject"></param>
        /// <returns></returns>
        public OperationResult addNewPhaseToProject(Phase newPhase, Project targetProject)
        {
            var p = FindProjectById(targetProject.ID);
            if (p == null)
                return OperationResult.OutOfSync;

            p.Phases.Add(newPhase);
            return SaveProject(p);
        }

        /// <summary>
        /// Update name of phase
        /// </summary>
        /// <param name="updatedPhase"></param>
        /// <returns></returns>
        public OperationResult updatePhase(Phase updatedPhase)
        {
            var proj = FindProject(Query.EQ("Phases._id", updatedPhase.ID));

            if (proj == null)
                return OperationResult.OutOfSync;

            var i = proj.Phases.FindIndex(_ => _.ID == updatedPhase.ID);
            proj.Phases[i].Name = updatedPhase.Name;
            return SaveProject(proj);
        }

        /// <summary>
        /// Remove phase from project
        /// </summary>
        /// <param name="removedPhase"></param>
        /// <returns></returns>
        public OperationResult removePhase(Phase removedPhase)
        {
            var proj = FindProject(Query.EQ("Phases._id", removedPhase.ID));

            if (proj == null)
                return OperationResult.OutOfSync;

            proj.Phases.RemoveAll(_ => _.ID == removedPhase.ID);
            //TODO:  Remove proj from deleted phase
            return SaveProject(proj);
        }

        /// <summary>
        /// Add new project for existing member
        /// </summary>
        /// <param name="newProject"></param>
        /// <param name="targetMember"></param>
        /// <returns></returns>
        public OperationResult addNewProjectForMember(Project newProject, Member targetMember)
        {
            newProject.Members.Add(targetMember.ID);

            var projectSaveResult = SaveProject(newProject);
            targetMember = FindMemberById(targetMember.ID);
            targetMember.Projects.Add(newProject.ID);
            var memberSaveResult = SaveMember(targetMember);
            return projectSaveResult & memberSaveResult;
        }

        /// <summary>
        /// Update name of project
        /// </summary>
        /// <param name="updatedProject"></param>
        /// <returns></returns>
        public OperationResult updateProject(Project updatedProject)
        {
            var p = FindProjectById(updatedProject.ID);
            
            if (p == null) return OperationResult.OutOfSync;

            p.Name = updatedProject.Name;
            p.Timestamp = updatedProject.Timestamp;
            return SaveProject(p);
        }

        /// <summary>
        /// Remove project and all its tasks. Even if tasks were updated.
        /// </summary>
        /// <param name="removedProject"></param>
        /// <returns></returns>
        public OperationResult removeProject(Project removedProject)
        {
            var proj = FindProjectById(removedProject.ID);

            if (proj == null)
                return OperationResult.OutOfSync;

            if (proj.Timestamp > removedProject.Timestamp)
                return OperationResult.OutOfSync;
            try
            {
                Projects.Remove(Query.EQ("_id", removedProject.ID));

                foreach (var phase in proj.Phases)
                    foreach (var taskId in phase.TaskIDs)
                    {
                        Tasks.Remove(Query.EQ("_id", taskId));
                    }
            }
            catch (MongoException me)
            {
                Logger.Write.Error("Can't delete project", me);
                return OperationResult.DatabaseError;
            }

            return OperationResult.Success;

        }
  
        /// <summary>
        /// Return project with specified ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Project projectWithID(ObjectId id)
        {
            return FindProjectById(id);
        }

        /// <summary>
        /// Return list of projects that ID belongs to parameter 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IList<Project> projectsWithIDs(IList<ObjectId> ids)
        {
            var result = new List<Project>();
            foreach (var id in ids)
            {
                result.Add(FindProject(Query.EQ("_id", id)));
            }
            return result;
        }

        /// <summary>
        /// Return list of projects that have been updated or created after time specified in parameter
        /// </summary>
        /// <param name="clientsTimestamps"></param>
        /// <returns></returns>
        public IList<Project> updatedProjects(Dictionary<ObjectId, DateTime> clientsTimestamps)
        {
            var newerProjects = new List<Project>();
            foreach (var t in clientsTimestamps.Keys)
            {
                var proj = FindProjectById(t);
                if (proj == null) continue;

                if (clientsTimestamps[t] < proj.Timestamp)
                {
                    newerProjects.Add(proj);
                }
            }

            return newerProjects;
        }

        #endregion

        /// <summary>
        /// Save member.
        /// </summary>
        /// <remarks>If member does not exist create new member</remarks>
        /// <param name="member"></param>
        /// <returns></returns>
        public OperationResult saveMember(Member member)
        {
            return SaveMember(member);
        }

        public Member memberWithLoginAndPassword(string login, string password)
        {
            var credentials = (from c in Credentials.AsQueryable()
                               where c.Login == login
                               select c);
            foreach (var c in credentials)
            {
                if (PasswordHash.ValidatePassword(password, c.Hash))
                    return memberWithID(c.MemberId);
            }
            return null;
        }

        public OperationResult saveNewMemberWithLoginAndPassword(Member member, string login, string password)
        {
            var credentials = (from c in Credentials.AsQueryable()
                               where c.Login == login
                               select c);

            foreach (var c in credentials)
            {
                return OperationResult.PriviligesError; //userwith this login already exists
            }

            var result = SaveMember(member);
            if (result != OperationResult.Success)
                return result;
            try {
                Credentials.Insert(new Credentials() { MemberId = member.ID, 
                                                       Login = login, 
                                                       Hash = PasswordHash.CreateHash(password)});
            } catch(MongoException me) {
                Logger.Write.Error("Adding new member failed", me);
                return OperationResult.DatabaseError;
            }
            return result;
        }
    }
}
