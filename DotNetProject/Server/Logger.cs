﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.IO;
using System.Configuration;

namespace Server
{
    public class Logger
    {
        public static ILog Write
        {
            get
            {
                try
                {
                    var confFile = ConfigurationSettings.AppSettings.Get("log4net.config");
                    var fi = new FileInfo(confFile);
                    XmlConfigurator.Configure(fi);
                }
                catch (Exception e) { }
                return LogManager.GetLogger("WebAppLog");
            }
        }
    }
}
