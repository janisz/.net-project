﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using Common.DTO;
using Common.OperationResult;
using System.ServiceModel;
using MongoDB.Driver.Linq;
using Common;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NewMember" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NewMember.svc or NewMember.svc.cs at the Solution Explorer and start debugging.
    public class NewMember : INewMember
    {
        MongoCollection<Credentials> Credentials;
        MongoCollection<Member> Members;
        MongoServer server;
        MongoDatabase db;

        public NewMember()
        {
            server = MongoServer.Create("mongodb://localhost");
            db = server.GetDatabase("dotNet");

            Members = db.GetCollection<Member>("members");
            Credentials = db.GetCollection<Credentials>("credentials");
        }

        private OperationResult SaveMember(Member member)
        {
            var oldMember = Members.FindOneById(member.ID);
            if (oldMember != null && oldMember.Timestamp > member.Timestamp)
            {
                Logger.Write.Debug("Outdated member, need to update data");
                return OperationResult.OutOfSync;
            }

            try
            {
                member.Timestamp = DateTime.Now;
                Members.Save(member);
                Logger.Write.Debug("Save Member: " + member.ID);
            }
            catch (MongoException me)
            {
                Logger.Write.Error(me.Message);
                return OperationResult.DatabaseError;
            }

            return OperationResult.Success;
        }

        public OperationResult saveNewMemberWithLoginAndPassword(Member member, string login, string password)
        {
            var credentials = (from c in Credentials.AsQueryable()
                               where c.Login == login
                               select c);

            foreach (var c in credentials)
            {
                return OperationResult.PriviligesError; //userwith this login already exists
            }

            var result = SaveMember(member);
            if (result != OperationResult.Success)
                return result;
            try
            {
                Credentials.Insert(new Credentials()
                {
                    MemberId = member.ID,
                    Login = login,
                    Hash = PasswordHash.CreateHash(password)
                });
            }
            catch (MongoException me)
            {
                Logger.Write.Error("Adding new member failed", me);
                return OperationResult.DatabaseError;
            }
            return result;
        }
    }
}
