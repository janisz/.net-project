﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Common.DTO
{
    /// <summary>
    /// Abstract superclass of all the concrete classes in the Data Transportation layer
    /// </summary>
    [DataContract] 
    public abstract class Model
    {
        [BsonId]
        [DataMember]
        public ObjectId ID { get; set; }

        [BsonElement]
        [DataMember]
        public string Name
        {
            get;
            set;
        }

        [BsonElement]
        [DataMember]
        public System.DateTime Timestamp
        {
            get;
            set;
        }

        public Model(string _name)
        {
            Name = _name;
            ID = ObjectId.GenerateNewId();
            Timestamp = System.DateTime.MinValue;
        }
    }
}
