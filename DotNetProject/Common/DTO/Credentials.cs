﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Common.DTO
{
    public class Credentials
    {
        [BsonId]      
        public ObjectId ID { get; set; }
        [BsonElement]
        public ObjectId MemberId;
        [BsonElement]
        public string Login;
        [BsonElement]
        public string Hash;
    }
}
