﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Common.DTO
{
    /// <summary>
    /// Represents a single task in the Data Transportation layer
    /// </summary>
    [DataContract] 
    public class Task : Model
    {

        string description = "Default task description";

        [BsonElement]
        [DataMember]
        public int Order // order in phase
        {
            get;
            set;
        }

        [BsonElement]
        [DataMember]
        public IList<ObjectId> Members = new List<ObjectId>();

        [BsonElement]
        [DataMember]
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        [BsonElement]
        [DataMember]
        public int Priority
        {
            get;
            set;
        }

        public Task(string taskName)
            : base(taskName)
        {
        }
    }
}
