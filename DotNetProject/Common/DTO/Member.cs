﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Common.DTO
{
    /// <summary>
    /// Represents a single member of a Task or Project in the Data Transportation layer
    /// </summary>
    [DataContract] 
    public class Member : Model
    {
        public Member(string _name, string _surname)  : base(_name)
        {
            Surname = _surname;
        }

        [BsonElement]
        [DataMember]
        public string Surname
        {
            get;
            set;
        }

        [BsonElement]
        [DataMember]
        public List<ObjectId> Projects = new List<ObjectId>();
    }
}
