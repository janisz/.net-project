﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Common.DTO
{
    /// <summary>
    /// Represents a single Phase of a Project in the Data Transportation layer
    /// </summary>
    [DataContract] 
    public class Phase : Model
    {
        [BsonElement]
        [DataMember]
        public List<ObjectId> TaskIDs = new List<ObjectId>();

        public Phase(string _name) : base(_name)
        {
        }
    }
}
