﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Common.DTO
{
    /// <summary>
    /// Represents a Project in the Data Transportation layer
    /// </summary>
    [DataContract] 
    public class Project : Model
    {
        [BsonElement]
        [DataMember]
        public List<Phase> Phases = new List<Phase>();


        [BsonElement]
        [DataMember]
        public List<ObjectId> Members = new List<ObjectId>();

        public Project(string projectName) : base(projectName)
        {
        }

    }
}
