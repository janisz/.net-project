﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.OperationResult
{
    /// <summary>
    /// Possible results with DB comunication
    /// </summary>
    public enum OperationResult
    {
        Success = 0,
        /// <summary>
        /// DB have been updated. Data are not saved
        /// </summary>
        OutOfSync = 1,
        DatabaseError = 2,
        PriviligesError = 4
    }
}
